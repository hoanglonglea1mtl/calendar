import 'package:flutter/material.dart';

class UIHelper {
  UIHelper._();

  /// Fill box
  static const fillBoxConstraint = BoxConstraints.expand();

  static const emptyBox = const SizedBox();

  /// Vertical Spacing
  static const verticalBox1 = const SizedBox(height: 1);
  static const verticalBox2 = const SizedBox(height: 2);
  static const verticalBox4 = const SizedBox(height: 4);
  static const verticalBox5 = const SizedBox(height: 5);
  static const verticalBox8 = const SizedBox(height: 8);
  static const verticalBox6 = const SizedBox(height: 6);
  static const verticalBox12 = const SizedBox(height: 12);
  static const verticalBox16 = const SizedBox(height: 16);
  static const verticalBox18 = const SizedBox(height: 18);
  static const verticalBox20 = const SizedBox(height: 20);
  static const verticalBox24 = const SizedBox(height: 24);
  static const verticalBox28 = const SizedBox(height: 28);
  static const verticalBox32 = const SizedBox(height: 32);
  static const verticalBox48 = const SizedBox(height: 48);
  static const verticalBox64 = const SizedBox(height: 64);

  /// Horizontal Spacing
  static const horizontalBox1 = const SizedBox(width: 1);
  static const horizontalBox2 = const SizedBox(width: 2);
  static const horizontalBox4 = const SizedBox(width: 4);
  static const horizontalBox5 = const SizedBox(width: 5);
  static const horizontalBox8 = const SizedBox(width: 8);
  static const horizontalBox6 = const SizedBox(width: 6);
  static const horizontalBox12 = const SizedBox(width: 12);
  static const horizontalBox16 = const SizedBox(width: 16);
  static const horizontalBox18 = const SizedBox(width: 18);
  static const horizontalBox20 = const SizedBox(width: 20);
  static const horizontalBox24 = const SizedBox(width: 24);
  static const horizontalBox28 = const SizedBox(width: 28);
  static const horizontalBox32 = const SizedBox(width: 32);
  static const horizontalBox48 = const SizedBox(width: 48);
  static const horizontalBox64 = const SizedBox(width: 64);

  /// Vertical EdgeInsets
  static const verticalEdgeInsets2 = const EdgeInsets.symmetric(vertical: 2);
  static const verticalEdgeInsets4 = const EdgeInsets.symmetric(vertical: 4);
  static const verticalEdgeInsets6 = const EdgeInsets.symmetric(vertical: 6);
  static const verticalEdgeInsets8 = const EdgeInsets.symmetric(vertical: 8);
  static const verticalEdgeInsets12 = const EdgeInsets.symmetric(vertical: 12);
  static const verticalEdgeInsets16 = const EdgeInsets.symmetric(vertical: 16);
  static const verticalEdgeInsets20 = const EdgeInsets.symmetric(vertical: 20);
  static const verticalEdgeInsets24 = const EdgeInsets.symmetric(vertical: 24);
  static const verticalEdgeInsets32 = const EdgeInsets.symmetric(vertical: 32);

  /// Horizontal EdgeInsets
  static const horizontalEdgeInsets2 = const EdgeInsets.symmetric(horizontal: 2);
  static const horizontalEdgeInsets4 = const EdgeInsets.symmetric(horizontal: 4);
  static const horizontalEdgeInsets6 = const EdgeInsets.symmetric(horizontal: 6);
  static const horizontalEdgeInsets8 = const EdgeInsets.symmetric(horizontal: 8);
  static const horizontalEdgeInsets12 = const EdgeInsets.symmetric(horizontal: 12);
  static const horizontalEdgeInsets16 = const EdgeInsets.symmetric(horizontal: 16);
  static const horizontalEdgeInsets20 = const EdgeInsets.symmetric(horizontal: 20);
  static const horizontalEdgeInsets24 = const EdgeInsets.symmetric(horizontal: 24);
  static const horizontalEdgeInsets32 = const EdgeInsets.symmetric(horizontal: 32);

  /// Padding EdgeInsets
  static const paddingAll2 = const EdgeInsets.all(2);
  static const paddingAll4 = const EdgeInsets.all(4);
  static const paddingAll6 = const EdgeInsets.all(6);
  static const paddingAll8 = const EdgeInsets.all(8);
  static const paddingAll12 = const EdgeInsets.all(12);
  static const paddingAll16 = const EdgeInsets.all(16);
  static const paddingAll24 = const EdgeInsets.all(24);
  static const paddingZero = const EdgeInsets.all(0);

  ///duration
  static const duration500 = const Duration(milliseconds: 500);
  static const duration250 = const Duration(milliseconds: 250);
  static const duration30Day = const Duration(days: 30);
}
