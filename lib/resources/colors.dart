import 'package:flutter/material.dart';

class MyColors {
  static const Color lightBlue = Color(0xFF5684AE);
  static const Color darkBlue = Color(0xFF0F4C81);
  static const Color lightOrange = Color(0xFFFFE4C8);
  static const Color darkOrange = Color(0xFFF9BE81);
  static const Color calendarTitle = Color(0xFFE4F6ED);
  static const Color white = Colors.white;
  static const Color black = Colors.black;

  static const MaterialColor darkBluePrimary = const MaterialColor(
    0xFF0F4C81,
    const <int, Color>{
      50: const Color(0xFF0F4C81),
      100: const Color(0xFF0F4C81F),
      200: const Color(0xFF0F4C81),
      300: const Color(0xFF0F4C81),
      400: const Color(0xFF0F4C81),
      500: const Color(0xFF0F4C81),
      600: const Color(0xFF0F4C81),
      700: const Color(0xFF0F4C81),
      800: const Color(0xFF0F4C81),
      900: const Color(0xFF0F4C81),
    },
  );
}
