import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';

import '../../../helpers/helpers.dart';
import '../../../bloc/bloc.dart';
import '../../../widget/widget.dart';
import '../../../resources/resources.dart';
import '../../../extensions/extensions.dart';

class CalendarMobilePage extends StatefulWidget {
  @override
  _CalendarMobilePageState createState() => _CalendarMobilePageState();
}

class _CalendarMobilePageState extends State<CalendarMobilePage> {
  CalendarController _controller = CalendarController();

  CalendarBloc _calendarBloc = CalendarBloc()..changeSelectDate(DateTime.now());

  @override
  void initState() {
    super.initState();
    _controller.addPropertyChangedListener((val) {
      _calendarBloc.changeDisplayDate(_controller?.displayDate);
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  bool iss = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: MyColors.calendarTitle,
        body: GestureDetector(
          onVerticalDragEnd: (val) {
            _calendarBloc?.swipeScreen(val.primaryVelocity > 0);
          },
          child: Container(
            padding: UIHelper.paddingAll16,
            child: StreamBuilder<CalendarState>(
                stream: _calendarBloc.stateStream,
                builder: (context, snapshot) {
                  return Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      UIHelper.verticalBox32,
                      Text(
                        (snapshot?.data?.displayDate ?? DateTime.now()).formatDateMMMMYYYY,
                        style: Theme.of(context).textTheme.headline5.textDarkBlue.weight(FontWeight.w900),
                      ),
                      UIHelper.verticalBox12,
                      Expanded(
                          child: Column(
                        children: [
                          Expanded(
                            child: ContainerCalendarTableMobile(
                              calendarController: _controller,
                              bloc: _calendarBloc,
                            ),
                          ),
                          ContainerDateTimePickerMobile(
                            calendarController: _controller,
                            bloc: _calendarBloc,
                          )
                        ],
                      )),
                    ],
                  );
                }),
          ),
        ));
  }
}
