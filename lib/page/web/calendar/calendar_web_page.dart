import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import '../../../helpers/helpers.dart';
import '../../../bloc/bloc.dart';
import '../../../widget/widget.dart';
import '../../../resources/resources.dart';

class CalendarWebPage extends StatefulWidget {
  @override
  _CalendarWebPageState createState() => _CalendarWebPageState();
}

class _CalendarWebPageState extends State<CalendarWebPage> {
  CalendarController _controller = CalendarController();

  CalendarBloc _calendarBloc = CalendarBloc()..changeSelectDate(DateTime.now());

  @override
  void initState() {
    super.initState();
    _controller.addPropertyChangedListener((val) {
      _calendarBloc.changeDisplayDate(_controller?.displayDate);
    });
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: MyColors.calendarTitle,
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: Dimens.size40, vertical: Dimens.size26),
        child: Row(
          children: [
            Container(
              width: MediaQuery.of(context).size.width / 4,
              height: double.infinity,
              constraints: BoxConstraints(maxWidth: 350),
              child: ContainerDateTimePicker(
                calendarController: _controller,
                bloc: _calendarBloc,
              ),
            ),
            UIHelper.horizontalBox20,
            Expanded(
              flex: DimensInt.size2,
              child: CalendarTable(
                calendarController: _controller,
                bloc: _calendarBloc,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
