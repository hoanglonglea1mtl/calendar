import 'package:flutter/material.dart';
import '../../resources/resources.dart';

class EventModel {
  EventModel(this.eventName, this.from, this.to, this.background, this.isAllDay, this.headerEvent, this.isMeeting);

  String eventName;
  DateTime from;
  DateTime to;
  Color background;
  Color headerEvent;
  bool isAllDay;
  bool isMeeting;

  static List<EventModel> getEventsDataSource() {
    final List<EventModel> _events = <EventModel>[];
    _events.addAll([
      EventModel('Meeting with Paul', DateTime(2021, 6, 15, 9, 0, 0), DateTime(2021, 6, 15, 12, 0, 0), MyColors.darkBlue, false, MyColors.lightOrange, true),
      EventModel('Webinar: How to fix bug and keywork search in Flutter', DateTime(2021, 6, 20, 13, 0, 0), DateTime(2021, 6, 20, 15, 0, 0), MyColors.lightOrange, false,
          MyColors.darkBlue, false),
      EventModel('Meeting with Paul', DateTime(2021, 6, 21, 16, 0, 0), DateTime(2021, 6, 21, 16, 0, 0), MyColors.darkOrange, false, MyColors.darkBlue, true),
      EventModel('Webinar: How to fix bug and keywork search in ReactNative', DateTime(2021, 6, 22, 9, 0, 0), DateTime(2021, 6, 22, 12, 0, 0), MyColors.lightOrange, false,
          MyColors.darkBlue, false),
      EventModel('Join the meeting with the director', DateTime(2021, 6, 22, 13, 0, 0), DateTime(2021, 6, 22, 15, 0, 0), MyColors.darkBlue, false, MyColors.lightOrange, true),
      EventModel('Webinar: How to fix bug and keywork search in Flutter', DateTime(2021, 6, 22, 16, 0, 0), DateTime(2021, 6, 22, 21, 0, 0), MyColors.darkOrange, false,
          MyColors.darkBlue, false),
      EventModel('Join the meeting with the director', DateTime(2021, 6, 27, 9, 0, 0), DateTime(2021, 6, 27, 12, 0, 0), MyColors.lightOrange, false, MyColors.darkBlue, true),
      EventModel('Webinar: How to fix bug and keywork search in Flutter', DateTime(2021, 6, 25, 13, 0, 0), DateTime(2021, 6, 25, 15, 0, 0), MyColors.darkBlue, false,
          MyColors.lightOrange, false),
      EventModel('Meeting with Paul', DateTime(2021, 6, 25, 16, 0, 0), DateTime(2021, 6, 25, 21, 0, 0), MyColors.darkOrange, false, MyColors.darkBlue, true),
    ]);
    return _events;
  }
}
