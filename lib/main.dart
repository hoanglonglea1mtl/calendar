import 'package:flutter/material.dart';
import './resources/resources.dart';
import './page/page.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Calendar',
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primarySwatch: MyColors.darkBluePrimary,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      home: kIsWeb ? CalendarWebPage() : CalendarMobilePage(),
    );
  }
}
