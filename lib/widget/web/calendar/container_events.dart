import 'package:flutter/material.dart';
import '../../../bloc/bloc.dart';
import '../../widget.dart';
import '../../../extensions/extensions.dart';
import '../../../helpers/helpers.dart';
import '../../../resources/resources.dart';

class ContainerEvents extends StatelessWidget {
  final CalendarBloc bloc;

  ContainerEvents({@required this.bloc});

  @override
  Widget build(BuildContext context) {
    final _textTheme = Theme.of(context).textTheme;
    return Container(
      child: StreamBuilder<CalendarState>(
        stream: bloc.stateStream,
        builder: (context, snapshot) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Text(
                    "Upcoming Events",
                    style: _textTheme.subtitle1.textDarkBlue.weight(FontWeight.w900),
                  ),
                  (snapshot?.data?.eventsByDate?.length ?? 0) > 0 ? _buildButtonViewAll(context) : UIHelper.emptyBox,
                ],
              ),
              UIHelper.verticalBox12,
              Text(
                snapshot?.data?.selectDate?.formatDateTime(DateExtension.dateFormatEEEddMMM) ?? "--",
                style: _textTheme.caption.textGrey.weight(FontWeight.w900),
              ),
              UIHelper.verticalBox12,
              Expanded(
                child: bloc.waiting
                    ? Loading()
                    : (snapshot?.data?.eventsByDate?.length ?? 0) == 0
                        ? NoDataWidget()
                        : SingleChildScrollView(
                            child: Column(
                              children: snapshot.data.eventsByDate.map((e) => ItemEvent(event: e)).toList(),
                            ),
                          ),
              ),
            ],
          );
        },
      ),
    );
  }

  Widget _buildButtonViewAll(BuildContext context) {
    return InkWell(
      onTap: () {},
      child: Container(
        alignment: Alignment.center,
        width: Dimens.size64,
        height: Dimens.size26,
        decoration: BoxDecoration(
          color: MyColors.darkBlue,
          borderRadius: BorderRadius.circular(Dimens.size40),
        ),
        child: Text(
          "View all",
          style: Theme.of(context).textTheme.caption.textWhite.medium,
        ),
      ),
    );
  }
}
