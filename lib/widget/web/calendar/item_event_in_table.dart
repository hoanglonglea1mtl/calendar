import 'package:flutter/material.dart';
import '../../../helpers/helpers.dart';
import '../../../resources/resources.dart';
import '../../../data/data.dart';
import '../../../extensions/extensions.dart';

class ItemEventInTable extends StatelessWidget {
  final EventModel event;

  ItemEventInTable({@required this.event});

  @override
  Widget build(BuildContext context) {
    final _themeData = Theme.of(context);
    return Container(
      width: double.infinity,
      height: Dimens.size100,
      margin: UIHelper.verticalEdgeInsets2,
      decoration: BoxDecoration(
        color: event.background,
        borderRadius: BorderRadius.only(
          topRight: Radius.circular(Dimens.size2),
          bottomRight: Radius.circular(Dimens.size2),
        ),
      ),
      child: Row(
        children: [
          Container(
            width: Dimens.size3,
            height: double.infinity,
            decoration: BoxDecoration(
              color: event.headerEvent,
            ),
          ),
          UIHelper.horizontalBox2,
          Expanded(
            child: Text(
              event?.eventName ?? "--",
              maxLines: 1,
              style: _themeData.textTheme.caption.thin.textColor(event?.headerEvent ?? MyColors.white).spacing(Dimens.size1).size(Dimens.typography10),
            ),
          ),
        ],
      ),
    );
  }
}
