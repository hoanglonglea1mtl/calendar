import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';

import '../../../enums.dart';
import '../../../bloc/bloc.dart';
import '../../../resources/resources.dart';
import '../../../extensions/extensions.dart';
import '../../../helpers/helpers.dart';
import '../../../data/data.dart';
import '../../widget.dart';

class CalendarTable extends StatefulWidget {
  final CalendarController calendarController;
  final CalendarBloc bloc;

  CalendarTable({@required this.calendarController, @required this.bloc});

  @override
  _CalendarTableState createState() => _CalendarTableState();
}

class _CalendarTableState extends State<CalendarTable> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<CalendarState>(
        stream: widget.bloc.stateStream,
        builder: (context, snapshot) {
          return Container(
            color: MyColors.white,
            child: Column(
              children: [
                _buildMonthAndNavigator(context, dateTime: snapshot?.data?.displayDate),
                UIHelper.verticalBox20,
                Expanded(
                  child: SfCalendar(
                    controller: widget.calendarController,
                    cellEndPadding: Dimens.size0,
                    headerHeight: Dimens.size0,
                    todayHighlightColor: MyColors.darkBlue,
                    view: _changeCalendarView(snapshot?.data?.view),
                    dataSource: EventDataSource(EventModel.getEventsDataSource()),
                    selectionDecoration: BoxDecoration(
                      color: _checkToday(DateTime.now(), snapshot?.data?.selectDate ?? DateTime.now()) ? Colors.transparent : MyColors.darkOrange.withOpacity(0.4),
                    ),
                    monthViewSettings: MonthViewSettings(
                        monthCellStyle: MonthCellStyle(
                          todayBackgroundColor: MyColors.calendarTitle,
                          leadingDatesTextStyle: TextStyle(
                            color: Colors.grey.withOpacity(0.5),
                          ),
                          trailingDatesTextStyle: TextStyle(
                            color: Colors.grey.withOpacity(0.5),
                          ),
                        ),
                        dayFormat: "EEE",
                        appointmentDisplayCount: DimensInt.size5,
                        appointmentDisplayMode: MonthAppointmentDisplayMode.appointment),
                    appointmentBuilder: (context, detail) {
                      List<EventModel> a = [];
                      detail.appointments.map((e) => a.add(e as EventModel)).toList();
                      return ItemEventInTable(
                        event: a[0],
                      );
                    },
                    onSelectionChanged: (val) {
                      widget.bloc?.changeSelectDate(val.date);
                    },
                  ),
                ),
              ],
            ),
          );
        });
  }

  Widget _buildMonthAndNavigator(BuildContext context, {@required DateTime dateTime}) {
    return Padding(
      padding: UIHelper.paddingAll12,
      child: Row(
        children: [
          _buildButtonToDay(context),
          UIHelper.horizontalBox12,
          InkWell(
            onTap: () {
              widget.calendarController.displayDate = (dateTime ?? DateTime.now()).subtract(UIHelper.duration30Day);
            },
            child: Icon(
              Icons.navigate_before,
              color: MyColors.darkBlue,
            ),
          ),
          UIHelper.horizontalBox12,
          InkWell(
            onTap: () {
              widget.calendarController.displayDate = (dateTime ?? DateTime.now()).add(UIHelper.duration30Day);
            },
            child: Icon(
              Icons.navigate_next,
              color: MyColors.darkBlue,
            ),
          ),
          UIHelper.horizontalBox12,
          Expanded(
            child: Container(
              alignment: Alignment.centerLeft,
              width: double.infinity,
              child: Text(
                (dateTime ?? DateTime.now()).formatDateMMMMYYYY,
                style: Theme.of(context).textTheme.subtitle1.textDarkBlue.weight(FontWeight.w900),
              ),
            ),
          ),
          ButtonChangeCalendarView(
            callBack: (val) {
              widget.bloc?.changeCalendarView(val);
            },
          )
        ],
      ),
    );
  }

  Widget _buildButtonToDay(BuildContext context) {
    return InkWell(
      onTap: () {
        widget.calendarController.displayDate = DateTime.now();
        widget.bloc.changeSelectDate(DateTime.now());
      },
      child: Container(
        alignment: Alignment.center,
        width: Dimens.size64,
        height: Dimens.size26,
        decoration: BoxDecoration(
          border: Border.all(color: MyColors.darkBlue),
          borderRadius: BorderRadius.circular(Dimens.size10),
        ),
        child: Text(
          "Today",
          style: Theme.of(context).textTheme.caption.textDarkBlue.medium,
        ),
      ),
    );
  }

  bool _checkToday(DateTime date1, DateTime date2) {
    return date1?.month == date2?.month && date2?.year == date1?.year && date1?.day == date2?.day;
  }

  CalendarView _changeCalendarView(CalendarViewEnum view) {
    print(view.toString());
    switch (view) {
      case CalendarViewEnum.DAY:
        return CalendarView.day;
        break;
      case CalendarViewEnum.MONTH:
        return CalendarView.month;
        break;
      case CalendarViewEnum.WEEK:
        return CalendarView.week;
        break;
      default:
        return CalendarView.month;
        break;
    }
  }
}
