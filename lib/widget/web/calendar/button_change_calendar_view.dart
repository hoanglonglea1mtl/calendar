import 'package:flutter/material.dart';
import '../../../resources/resources.dart';
import '../../../extensions/extensions.dart';
import '../../../helpers/helpers.dart';
import '../../../enums.dart';

class ButtonChangeCalendarView extends StatefulWidget {
  final Function(CalendarViewEnum) callBack;

  ButtonChangeCalendarView({this.callBack});

  @override
  _ButtonChangeCalendarViewState createState() => _ButtonChangeCalendarViewState();
}

class _ButtonChangeCalendarViewState extends State<ButtonChangeCalendarView> {
  String _value;

  @override
  void initState() {
    super.initState();
    _value = CalendarViewEnum.MONTH.value;
  }

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        _showPopupMenu();
      },
      child: Container(
        alignment: Alignment.center,
        padding: UIHelper.paddingAll4,
        width: Dimens.size70,
        height: Dimens.size26,
        decoration: BoxDecoration(
          color: MyColors.darkBlue,
          borderRadius: BorderRadius.circular(Dimens.size10),
        ),
        child: Row(
          children: [
            Expanded(
              child: Text(
                _value ?? "--",
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.caption.textWhite.thin,
              ),
            ),
            UIHelper.horizontalBox4,
            const Icon(
              Icons.keyboard_arrow_down,
              color: MyColors.white,
              size: Dimens.size16,
            ),
          ],
        ),
      ),
    );
  }

  void _showPopupMenu() async {
    await showMenu(
      context: context,
      position: const RelativeRect.fromLTRB(Dimens.size100, Dimens.size56, Dimens.size0, Dimens.size0),
      items: CalendarViewEnum.values
          .map(
            (e) => PopupMenuItem<String>(
              child: Align(
                alignment: Alignment.center,
                child: Text(
                  e.value,
                  textAlign: TextAlign.center,
                ),
              ),
              value: e.value,
            ),
          )
          .toList(),
      elevation: Dimens.size8,
    ).then((value) {
      if (value != null) {
        setState(() {
          _value = value;
        });
        widget.callBack?.call(_changeCalendarViewEnum(value));
      }
    });
  }

  CalendarViewEnum _changeCalendarViewEnum(String view) {
    switch (view) {
      case "Day":
        return CalendarViewEnum.DAY;
        break;
      case "Month":
        return CalendarViewEnum.MONTH;
        break;
      case "Week":
        return CalendarViewEnum.WEEK;
        break;
      default:
        return CalendarViewEnum.MONTH;
        break;
    }
  }
}
