import 'package:flutter/material.dart';
import '../../../constanst.dart';
import '../../../helpers/helpers.dart';
import '../../../resources/resources.dart';
import '../../../data/data.dart';
import '../../../extensions/extensions.dart';

class ItemEvent extends StatelessWidget {
  final EventModel event;

  ItemEvent({@required this.event});

  ThemeData _themeData;

  @override
  Widget build(BuildContext context) {
    _themeData = Theme.of(context);
    return Container(
      width: double.infinity,
      height: Dimens.size100,
      margin: UIHelper.verticalEdgeInsets8,
      decoration: BoxDecoration(
        color: event.background,
        borderRadius: BorderRadius.circular(Dimens.size8),
      ),
      child: Row(
        children: [
          Container(
            width: Dimens.size6,
            height: double.infinity,
            decoration: BoxDecoration(
              color: event.headerEvent,
              borderRadius: BorderRadius.only(
                topLeft: Radius.circular(Dimens.size20),
                bottomLeft: Radius.circular(Dimens.size20),
              ),
            ),
          ),
          Expanded(
            child: Padding(
              padding: UIHelper.paddingAll12,
              child: Row(
                children: [
                  _buildContent(),
                  _buildButtonMeeting(),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget _buildContent() {
    return Expanded(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            event?.eventName ?? "--",
            style: _themeData.textTheme.caption.semiBold.textColor(event?.headerEvent ?? MyColors.white).spacing(Dimens.size1),
          ),
          Text(
            "${event.from.formatTimeHHMMA} - ${event.to.formatTimeHHMMA} UTC ${event.from.timeZone}+",
            style: _themeData.textTheme.caption.thin.textColor(event?.headerEvent ?? MyColors.white),
          ),
          (event?.isMeeting ?? false)
              ? Row(
                  children: [
                    Container(
                      width: Dimens.size26,
                      height: Dimens.size26,
                      decoration: BoxDecoration(
                        color: MyColors.white,
                        shape: BoxShape.circle,
                        image: DecorationImage(
                          image: AssetImage(ImageConstants.avatarMeeting),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    UIHelper.horizontalBox12,
                    InkWell(
                      onTap: () {},
                      child: Text(
                        "View Client Profile",
                        style: _themeData.textTheme.caption.thin.textColor(event?.headerEvent ?? MyColors.white).copyWith(
                              decoration: TextDecoration.underline,
                            ),
                      ),
                    ),
                  ],
                )
              : UIHelper.emptyBox,
        ],
      ),
    );
  }

  Widget _buildButtonMeeting() {
    return Container(
      alignment: Alignment.topRight,
      width: Dimens.size50,
      height: double.infinity,
      child: (event?.isMeeting ?? false)
          ? Container(
              width: Dimens.size32,
              height: Dimens.size32,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color: event.headerEvent ?? MyColors.lightOrange,
              ),
              child: Icon(
                Icons.videocam_outlined,
                color: event?.background ?? MyColors.white,
              ),
            )
          : UIHelper.emptyBox,
    );
  }
}
