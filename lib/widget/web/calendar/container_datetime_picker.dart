import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import 'container_date_time.dart';
import 'container_events.dart';
import '../../../bloc/bloc.dart';
import '../../../resources/resources.dart';
import '../../../helpers/helpers.dart';

class ContainerDateTimePicker extends StatefulWidget {
  final CalendarController calendarController;
  final CalendarBloc bloc;

  ContainerDateTimePicker({this.calendarController, this.bloc});

  @override
  _ContainerDateTimePickerState createState() => _ContainerDateTimePickerState();
}

class _ContainerDateTimePickerState extends State<ContainerDateTimePicker> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: MyColors.white,
      padding: UIHelper.paddingAll24,
      child: Column(
        children: [
          Expanded(
            flex: DimensInt.size2,
            child: ContainerDateTime(
              bloc: widget.bloc,
              calendarController: widget.calendarController,
            ),
          ),
          Container(
            width: double.infinity,
            color: Colors.grey.withOpacity(0.3),
            height: Dimens.size3,
          ),
          UIHelper.verticalBox12,
          Expanded(
            flex: DimensInt.size3,
            child: ContainerEvents(
              bloc: widget.bloc,
            ),
          )
        ],
      ),
    );
  }
}
