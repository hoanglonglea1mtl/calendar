import 'package:flutter/material.dart';

import '../../resources/resources.dart';
import '../../extensions/extensions.dart';

class NoDataWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      width: double.infinity,
      height: double.infinity,
      child: Text(
        "No Data",
        style: TextStyle(color: MyColors.darkBlue).regular.size(Dimens.typography16),
      ),
    );
  }
}
