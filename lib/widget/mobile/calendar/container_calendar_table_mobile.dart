import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import '../../../bloc/bloc.dart';
import '../../../resources/resources.dart';
import '../../../extensions/extensions.dart';
import '../../../data/data.dart';
import '../../widget.dart';
import '../../../helpers/helpers.dart';

class ContainerCalendarTableMobile extends StatelessWidget {
  final CalendarController calendarController;
  final CalendarBloc bloc;

  ContainerCalendarTableMobile({@required this.calendarController, @required this.bloc});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<CalendarState>(
      stream: bloc.stateStream,
      builder: (context, snapshot) {
        return AnimatedOpacity(
          duration: UIHelper.duration500,
          opacity: (snapshot?.data?.swipeUpScreen ?? false) ? 1 : 0,
          child: SfCalendar(
            backgroundColor: Colors.transparent,
            controller: calendarController,
            cellEndPadding: Dimens.size0,
            headerHeight: Dimens.size0,
            todayHighlightColor: MyColors.darkBlue,
            view: CalendarView.month,
            dataSource: EventDataSource(EventModel.getEventsDataSource()),
            selectionDecoration: BoxDecoration(
              color: DateTime.now().checkEqualDate(
                (snapshot?.data?.selectDate ?? DateTime.now()),
              )
                  ? Colors.transparent
                  : MyColors.darkOrange.withOpacity(0.4),
            ),
            monthViewSettings: MonthViewSettings(
                monthCellStyle: MonthCellStyle(
                  todayBackgroundColor: MyColors.darkBlue,
                  leadingDatesTextStyle: TextStyle(
                    color: Colors.grey.withOpacity(0.5),
                  ),
                  trailingDatesTextStyle: TextStyle(
                    color: Colors.grey.withOpacity(0.5),
                  ),
                ),
                dayFormat: "EEE",
                appointmentDisplayCount: DimensInt.size5,
                appointmentDisplayMode: MonthAppointmentDisplayMode.appointment),
            appointmentBuilder: (context, detail) {
              List<EventModel> a = [];
              detail.appointments.map((e) => a.add(e as EventModel)).toList();
              return ItemEventInTableMobile(event: a[0]);
            },
            onSelectionChanged: (val) {
              bloc?.changeSelectDate(val.date);
            },
          ),
        );
      },
    );
  }
}
