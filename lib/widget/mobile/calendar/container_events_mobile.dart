import 'package:flutter/material.dart';
import '../../../bloc/bloc.dart';
import '../../widget.dart';
import '../../../extensions/extensions.dart';
import '../../../helpers/helpers.dart';

class ContainerEventsMobile extends StatelessWidget {
  final CalendarBloc bloc;

  ContainerEventsMobile({@required this.bloc});

  @override
  Widget build(BuildContext context) {
    final _textTheme = Theme.of(context).textTheme;
    return Container(
      child: StreamBuilder<CalendarState>(
        stream: bloc.stateStream,
        builder: (context, snapshot) {
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Text(
                "Upcoming Events",
                style: _textTheme.headline6.textDarkBlue.weight(FontWeight.w900),
              ),
              UIHelper.verticalBox12,
              Text(
                snapshot?.data?.selectDate?.formatDateTime(DateExtension.dateFormatEEEddMMM) ?? "--",
                style: _textTheme.subtitle2.textGrey.medium,
              ),
              UIHelper.verticalBox12,
              Expanded(
                child: bloc.waiting
                    ? Loading()
                    : (snapshot?.data?.eventsByDate?.length ?? 0) == 0
                        ? NoDataWidget()
                        : SingleChildScrollView(
                            child: Column(
                              children: snapshot.data.eventsByDate.map((e) => ItemEvent(event: e)).toList(),
                            ),
                          ),
              ),
            ],
          );
        },
      ),
    );
  }
}
