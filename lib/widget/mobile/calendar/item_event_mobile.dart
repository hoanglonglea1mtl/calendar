import 'package:flutter/material.dart';
import '../../../helpers/helpers.dart';
import '../../../resources/resources.dart';
import '../../../data/data.dart';
import '../../../extensions/extensions.dart';

class ItemEventInTableMobile extends StatelessWidget {
  final EventModel event;

  ItemEventInTableMobile({@required this.event});

  @override
  Widget build(BuildContext context) {
    final _themeData = Theme.of(context);
    return Container(
      alignment: Alignment.centerLeft,
      width: double.infinity,
      padding: UIHelper.horizontalEdgeInsets2,
      decoration: BoxDecoration(
        color: event.background,
        borderRadius: BorderRadius.circular(Dimens.size2),
      ),
      child: Text(
        event?.eventName ?? "--",
        maxLines: 1,
        style: _themeData.textTheme.caption.thin.textColor(event?.headerEvent ?? MyColors.white).spacing(Dimens.size1).size(Dimens.typography10),
      ),
    );
  }
}
