import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import '../../../bloc/bloc.dart';
import '../../../resources/resources.dart';
import '../../../extensions/extensions.dart';

class ContainerDateTimeMobile extends StatelessWidget {
  final CalendarBloc bloc;
  final CalendarController calendarController;

  ContainerDateTimeMobile({@required this.bloc, @required this.calendarController});

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<CalendarState>(
      stream: bloc.stateStream,
      builder: (context, snapshot) {
        return SfCalendar(
          controller: calendarController,
          view: CalendarView.month,
          backgroundColor: Colors.transparent,
          monthCellBuilder: (context, detail) {
            return Container(
              alignment: Alignment.center,
              decoration: BoxDecoration(
                shape: BoxShape.circle,
                color:
                    detail.date.checkEqualDate(DateTime.now()) ? MyColors.darkBlue : ((bloc?.checkIsHaveEvent(detail.date) ?? false) ? MyColors.lightOrange : Colors.transparent),
              ),
              child: Text(
                detail.date.day.toString(),
                style: TextStyle(color: detail.date.checkEqualDate(DateTime.now()) ? MyColors.white : MyColors.black),
              ),
            );
          },
          cellEndPadding: Dimens.size0,
          headerHeight: Dimens.size0,
          monthViewSettings: MonthViewSettings(
            appointmentDisplayCount: DimensInt.size0,
            showTrailingAndLeadingDates: false,
            dayFormat: "EEE",
          ),
          onSelectionChanged: (val) {
            bloc.changeSelectDate(val.date);
          },
          selectionDecoration: BoxDecoration(
            color: DateTime.now().checkEqualDate(snapshot?.data?.selectDate ?? DateTime.now()) ? Colors.transparent : MyColors.darkOrange.withOpacity(0.4),
            shape: BoxShape.circle,
          ),
        );
      },
    );
  }
}
