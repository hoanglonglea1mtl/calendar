import 'package:flutter/material.dart';
import 'package:syncfusion_flutter_calendar/calendar.dart';
import '../../widget.dart';
import '../../../bloc/bloc.dart';
import '../../../resources/resources.dart';
import '../../../helpers/helpers.dart';

class ContainerDateTimePickerMobile extends StatefulWidget {
  final CalendarController calendarController;
  final CalendarBloc bloc;

  ContainerDateTimePickerMobile({this.calendarController, this.bloc});

  @override
  _ContainerDateTimePickerMobileState createState() => _ContainerDateTimePickerMobileState();
}

class _ContainerDateTimePickerMobileState extends State<ContainerDateTimePickerMobile> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<CalendarState>(
      stream: widget.bloc.stateStream,
      builder: (context, snapshot) {
        return AnimatedContainer(
          duration: UIHelper.duration500,
          height: !(snapshot?.data?.swipeUpScreen ?? false) ? MediaQuery.of(context).size.height - Dimens.size110 : 0,
          child: Column(
            children: [
              Expanded(
                flex: DimensInt.size2,
                child: ContainerDateTimeMobile(
                  bloc: widget.bloc,
                  calendarController: widget.calendarController,
                ),
              ),
              Container(
                width: double.infinity,
                color: Colors.grey.withOpacity(0.3),
                height: Dimens.size3,
              ),
              UIHelper.verticalBox12,
              Expanded(
                flex: DimensInt.size3,
                child: ContainerEventsMobile(
                  bloc: widget.bloc,
                ),
              )
            ],
          ),
        );
      },
    );
  }
}
