import 'package:flutter/foundation.dart';
import 'package:rxdart/rxdart.dart';

abstract class BaseBloc<T extends dynamic> {
  BaseBloc({T state}) {
    _controller = BehaviorSubject<T>.seeded(state);
  }

  BehaviorSubject<T> _controller;
  Stream<T> get stateStream => _controller?.stream;
  final BehaviorSubject<bool> _waitingController = BehaviorSubject<bool>.seeded(false);
  T get state => _controller?.value;

  @mustCallSuper
  void dispose() {
    _waitingController.close();
    _controller?.close();
  }

  Stream<bool> get waitingStream => _waitingController?.stream;

  bool get waiting => _waitingController?.stream?.value;

  void emitWaiting(bool waiting) {
    if(_waitingController?.isClosed ?? true) return;
    _waitingController?.sink?.add(waiting);
  }

  @mustCallSuper
  void onResume() {}

  @mustCallSuper
  void onPause() {}

  @mustCallSuper
  void onDetach() {}

  @mustCallSuper
  void onInactive() {}

  void emit(dynamic state) => _controller?.sink?.add(state);
}
