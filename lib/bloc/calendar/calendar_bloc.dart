import '../../enums.dart';
import '../bloc.dart';
import '../../helpers/helpers.dart';
import '../../data/data.dart';
import '../../extensions/extensions.dart';

class CalendarBloc extends BaseBloc<CalendarState> {
  void changeDisplayDate(DateTime date) {
    emit(CalendarState(state: state, displayDate: date));
  }

  void changeSelectDate(DateTime date) {
    emit(CalendarState(state: state, selectDate: date));
    _getEventsByDate(dateTime: date);
  }

  void changeCalendarView(CalendarViewEnum view) {
    emit(CalendarState(state: state, view: view));
  }

  void swipeScreen(bool isUp) {
    emit(CalendarState(state: state, swipeUpScreen: isUp));
  }

  void _getEventsByDate({DateTime dateTime}) async {
    emitWaiting(true);
    await Future.delayed(UIHelper.duration500);
    List<EventModel> _events = [];
    EventModel.getEventsDataSource().forEach((element) {
      if (dateTime.checkEqualDate(element.from)) _events.add(element);
    });
    emitWaiting(false);
    emit(CalendarState(state: state, eventsByDate: _events));
  }

  bool checkIsHaveEvent(DateTime dateTime) {
    var _isHave = false;
    EventModel.getEventsDataSource().forEach((element) {
      if (dateTime.checkEqualDate(element.from)) {
        _isHave = true;
      }
    });
    return _isHave;
  }
}
