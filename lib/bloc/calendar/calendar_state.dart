import 'package:equatable/equatable.dart';

import '../../enums.dart';
import '../../data/data.dart';

class CalendarState extends Equatable {
  final List<EventModel> eventsByDate;
  final DateTime displayDate;
  final DateTime selectDate;
  final CalendarViewEnum view;
  final bool swipeUpScreen;

  CalendarState({CalendarState state, List<EventModel> eventsByDate, DateTime displayDate, CalendarViewEnum view, DateTime selectDate, bool swipeUpScreen})
      : this.displayDate = displayDate ?? state?.displayDate,
        this.eventsByDate = eventsByDate ?? state?.eventsByDate,
        this.selectDate = selectDate ?? state?.selectDate,
        this.swipeUpScreen = swipeUpScreen ?? state?.swipeUpScreen,
        this.view = view ?? state?.view;

  @override
  List<Object> get props => [displayDate, eventsByDate.hashCode, view, selectDate, swipeUpScreen];
}
