import 'package:intl/intl.dart';

extension DateExtension on DateTime {
  static const timeFormatHHMMA = 'HH:mm a';
  static const timeFormatHHMM = 'HH:mm';
  static const dateFormatMMMddYYY = 'MMM dd, yyyy';
  static const dateFormatddMMMMYYY = 'dd MMMM, yyyy';
  static const dateFormatMMMMYYYY = 'MMMM yyyy';
  static const dateFormatEEEddMMM = 'EEE,dd MMM';
  static const dateFormatddMMyyyy = 'dd/MM/yyyy';
  static const dateFormatHHMMddMMMyyyy = 'HH:mm - dd MMMM yyyy';
  static const dateFormatHHMMddMMyyyy = 'HH:mm dd/MM/yyyy';
  static const dateFormatyyyyMMdd = 'yyyy-MM-dd';

  String get formatTimeHHMM => DateFormat(timeFormatHHMM).format(this ?? DateTime.now());

  String get formatDateMMMMYYYY => DateFormat(dateFormatMMMMYYYY).format(this ?? DateTime.now());

  String get formatTimeHHMMA => DateFormat(timeFormatHHMMA).format(this ?? DateTime.now());

  String get formatTimeHHMMddMMMyyyy => DateFormat(dateFormatHHMMddMMMyyyy).format(this ?? DateTime.now());

  String get formatDateMMMddYYY => DateFormat(dateFormatMMMddYYY).format(this ?? DateTime.now());

  String get formatDateFormatddMMMMYYY => DateFormat(dateFormatddMMMMYYY).format(this ?? DateTime.now());

  String get formatDateddMMyyyy => DateFormat(dateFormatddMMyyyy).format(this ?? DateTime.now());

  String get formatDateyyyyMMdd => DateFormat(dateFormatyyyyMMdd).format(this ?? DateTime.now());

  String get formatDateHHMMddMMyyyy => DateFormat(dateFormatHHMMddMMyyyy).format(this ?? DateTime.now());

  int get timeZone => (this ?? DateTime.now()).timeZoneOffset.inHours;

  String formatDateTime(String formatter) => DateFormat(formatter).format(this ?? DateTime.now());

  bool checkEqualDate(DateTime date) => date?.month == this?.month && this?.year == date?.year && date?.day == this?.day;
}
