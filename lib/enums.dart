enum CalendarViewEnum {MONTH,WEEK,DAY}

extension CalendarViewExt on CalendarViewEnum {
  static List<String> get textValues => [ 'Month', 'Week', 'Day'];

  String get value => textValues[index];
}
